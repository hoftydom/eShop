package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;

/**
 * Auxiliary class for item storage
 */
public class ItemStock {
    private Item refItem;
    private int count;
    
    ItemStock(Item refItem) {
        this.refItem = refItem;
        count = 0;
    }
    
    @Override
    public String toString() {
        return "STOCK OF ITEM:  "+refItem.toString()+"    PIECES IN STORAGE: "+count;
    }
    
    void IncreaseItemCount(int x) {
        count += x;
    }
    
    void decreaseItemCount(int x) {
        if(count - x >= 0)
            count -= x;
        else
            System.err.println("ItemStock@decreaseItemCount: Trying to decrease count to negative value!");
    }
    
    int getCount() {
        return count;
    }
    
    Item getItem() {
        return refItem;
    }
}