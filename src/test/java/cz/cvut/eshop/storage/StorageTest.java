package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class StorageTest {

    private Storage storage;
    private HashMap<Integer, ItemStock> stockMap;
    private static List<Item> items;

    @BeforeClass
    public static void prepare() {
        items = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            items.add(new StandardItem(i, "item" + i, 100 + i, i < 3 ? "1" : "2", 0));
        }
    }

    @Before
    public void init() {
        stockMap = new HashMap<>();
        storage = new Storage(stockMap);
    }


    @Test
    public void insertItemsTest() {
        storage.insertItems(items.get(0), 1);
        assertTrue(stockMap.containsKey(items.get(0).getID()));

        for (int i = 0; i < 4; i++) {
            storage.insertItems(items.get(i + 1), 1);
        }
        assertTrue("Storage does not contain all inserted items", stockMap.keySet().containsAll(items.stream().map(item -> item.getID()).collect(Collectors.toList())));
    }

    @Test
    public void insertAlreadyContainedItemTest() {
        storage.insertItems(items.get(0), 1);
        String expectedItemName = items.get(0).getName();
        // try to insert different object but with same id -> expected result: should not be inserted
        Item sameIdDifferentItem = new StandardItem(items.get(0).getID(), "blableblu", 0, "bliblablo", 0);
        storage.insertItems(sameIdDifferentItem, 1);
        assertEquals(stockMap.get(items.get(0).getID()).getItem().getName(), expectedItemName);
    }


    @Test
    public void removeItemsTest() {
        for (int i = 0; i < 5; i++) {
            storage.insertItems(items.get(i), 1);
        }
        assertEquals("Insertion of elements into storage failed!!", stockMap.size(), 5);

        Item itemToBeRemoved = items.get(items.size() - 1);
        //storage.printListOfStoredItems();

        try {
            storage.removeItems(itemToBeRemoved, 1);
        } catch (NoItemInStorage noItemInStorage) {
            noItemInStorage.printStackTrace();
        }
        //storage.printListOfStoredItems();

        /*
         * As seen in print messages, storage still contains the ItemStock even though its count of items is 0.
         * In my opinion, an ItemStock with 0 items should not be contained in the storage. Therefore,
         * I have modified the Storage@removeItems method so that if count of items of an ItemStock is 0, it removes
         * the ItemStock from storage completely.
         *
         * After such modification, this test successfully passes
         */
        assertFalse("Storage still contains item that was already removed!", stockMap.containsKey(itemToBeRemoved.getID()));

    }

    @Test
    public void processOrderTest() {

    }

    @Test
    public void getItemCountByItemTest() {
        for (int i = 0; i < 5; i++) {
            storage.insertItems(items.get(i), i);
        }

        for (int i = 0; i < 5; i++) {
            assertEquals(storage.getItemCount(items.get(i)), i);
        }
    }

    @Test
    public void getItemCountByIDTest() {
        for (int i = 0; i < 5; i++) {
            storage.insertItems(items.get(i), i);
        }

        for (int i = 0; i < 5; i++) {
            assertEquals(storage.getItemCount(items.get(i).getID()), i);
        }
    }

    @Test
    public void getPriceOfWholeStockTest() {
        for (int i = 0; i < 5; i++) {
            storage.insertItems(items.get(i), i);
        }
        // storage.printListOfStoredItems();

        int expectedTotalPrice = 0;
        for (ItemStock itemStock : storage.getStockEntries()) {
            expectedTotalPrice += itemStock.getItem().getPrice() * itemStock.getCount();
        }

        /* Found a bug in getPriceOfWholeStock method: price of item in stock must be multiplied by its count.
         * Modification was made in Storage@getPriceOfWholeStock#126
         */
        assertEquals(expectedTotalPrice, storage.getPriceOfWholeStock());
    }

    @Test
    public void getItemsOfCategorySortedByPriceTest() {
        for (int i = 0; i < 5; i++) {
            storage.insertItems(items.get(i), i);
        }
        List<Item> itemsCopy = new ArrayList<>(items);

        List<Item> expectedForCategory1 = items.stream().filter(item -> item.getCategory() == "1").collect(Collectors.toList());
        List<Item> expectedForCategory2 = items.stream().filter(item -> item.getCategory() == "2").collect(Collectors.toList());

        // lazy to declare comparator explicitly
        expectedForCategory1.sort((o1, o2) -> {
            if (o1.getPrice() > o2.getPrice()) {
                return 1;
            } else if (o1.getPrice() > o2.getPrice()) {
                return 0;
            }
            return -1;
        });
        expectedForCategory2.sort((o1, o2) -> {
            if (o1.getPrice() > o2.getPrice()) {
                return 1;
            } else if (o1.getPrice() > o2.getPrice()) {
                return 0;
            }
            return -1;
        });


        List<Item> category1sorted = new ArrayList<>(storage.getItemsOfCategorySortedByPrice("1"));
        List<Item> category2sorted = new ArrayList<>(storage.getItemsOfCategorySortedByPrice("2"));

        for(int i = 0; i < expectedForCategory1.size(); i++) {
            assertEquals(expectedForCategory1.get(0), category1sorted.get(0));
        }
        for(int i = 0; i < expectedForCategory2.size(); i++){
            assertEquals(expectedForCategory2.get(0), category2sorted.get(0));
        }

    }

}
