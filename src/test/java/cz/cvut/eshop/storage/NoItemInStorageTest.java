package cz.cvut.eshop.storage;

import org.junit.Test;

public class NoItemInStorageTest {

   /*
   // also working
    @Test
    public void actuallyThrowsThatExceptionTest(){
        assertThatThrownBy(() -> {throw new NoItemInStorage(); })
                .isInstanceOf(Exception.class);
    }*/

    @Test(expected = Exception.class)
    public void actuallyThrowsThatExceptionAnotherApproachTest() throws Exception{
        throw new NoItemInStorage();
    }
}
