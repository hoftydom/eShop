package cz.cvut.eshop.storage;

import cz.cvut.eshop.shop.DiscountedItem;
import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ItemStockTest {

    private static ItemStock standardItemStock;
    private static ItemStock discountedItemStock;
    private static Item standardItem;
    private static Item discountedItem;

    @BeforeClass
    public static void initItems(){
        standardItem = new StandardItem(1, "ZKS Scriptum", 1500, "nonexisting stuff", 0);
        discountedItem = new DiscountedItem(1, "ZKS Scriptum", 1500, "nonexisting stuff", 1000, "11.11.2011", "12.12.2011");
    }

    @Before
    public void initItemStocks(){
        standardItemStock = new ItemStock(standardItem);
        discountedItemStock = new ItemStock(discountedItem);
    }

    @Test
    public void initialItemStockCountsTest(){
        assertEquals(0, standardItemStock.getCount());
        assertEquals(0, discountedItemStock.getCount());
    }

    @Test
    public void increaseItemStockCountTest(){
        standardItemStock.IncreaseItemCount(5);
        assertEquals(standardItemStock.getCount(),5);
        for (int i = 0; i < 50; i++) {
            discountedItemStock.IncreaseItemCount(2);
        }
        assertEquals(discountedItemStock.getCount(), 100);
    }

    @Test
    public void decreaseItemStockCountTest(){
        standardItemStock.IncreaseItemCount(55);
        discountedItemStock.IncreaseItemCount(10);
        assertEquals("Failed to increase standard item stock count!", standardItemStock.getCount(), 55);
        assertEquals("Failed to increase discounted item stock count!", discountedItemStock.getCount(), 10);

        for (int i = 0; i < 8; i++) {
            standardItemStock.decreaseItemCount(3);
        }
        assertEquals(standardItemStock.getCount(),31);

        discountedItemStock.decreaseItemCount(7);
        assertEquals(3,discountedItemStock.getCount());
    }

    @Test
    /**
     * I have modified the code in Item class so that this method passes:
     * added condition to decreaseItemCount(int x) method
     */
    public void nonNegativeItemStockCountTest(){
        for (int i = 0; i < 5; i++) {
            standardItemStock.decreaseItemCount(1);
        }
        assertTrue("ItemStock count should  not be negative!",standardItemStock.getCount() >= 0);

    }

}
